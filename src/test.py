import sys, os
import subprocess

import io
from turtle import position
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import style

#%% INPUTS

algorithm = 'sa'
n_exe = 100

problem_dir = os.path.join('instances_mknap')
instance_dir = os.path.join(problem_dir,os.listdir(problem_dir)[4])

# Knapsack fitness penalization (only applies in GA)
'''
0: death penalty -> f = 0.0
1: f = capacity - f*0.1
'''
kns_penalty = 0

# SA
ntemp = 3.0 # temp = ntemp*max(kns_p)
alpha = 0.9

# SA tuning
sa_tuning = {
        'temp': [1.0, 2.0, 3.0],
        'alpha': [0.85,0.9,0.95]
}

# GA
npm = 1.05 # pm = npm/kns_size
npsz = 3 # popsize = kns_size*npz
pc = 0.95

# GA tuning
ga_tuning = {
        'npsz': [1,2,3],
        'pc': [0.85,0.9,0.95],
        'npm': [0.95,1.0,1.05]
}

#%% Java functions

if algorithm != 'plot':

    def compile_java (java_file):
        subprocess.check_call(['javac', java_file])

    def execute_java (java_file, inputs):
        cmd='java' + ' ' + java_file + ' '  + inputs
        proc=subprocess.run(cmd, capture_output=True)
        return proc.stdout.decode()

    compile_java('ga/ssGA/*.java')
    compile_java('sa/*.java')


#%% Multidemnsional knapsack problem instance

if algorithm not in ['all','plot','tuning_sa','tuning_ga']:

    txt_file = open(instance_dir,'r')
    txt_lines = txt_file.readlines()
    txt_file.close()

    kns_size = int(txt_lines[0].split()[0])
    kns_ncons = int(txt_lines[0].split()[1])
    kns_solution = float(txt_lines[0].split()[2])
    kns_p = [float(i) for i in list(txt_lines[1].split())]
    kns_capacity = [float(i) for i in list(txt_lines[-1].split())]

    kns_w = []
    for i in range(1,len(txt_lines)-1):
        kns_w.append([float(j) for j in list(txt_lines[i].split())])

    # Flatten list of list
    kns_w = [item for sublist in kns_w for item in sublist]

    inputs_kns = str([kns_size, kns_ncons, kns_solution, kns_penalty] + kns_p + kns_capacity + kns_w).replace(',',' ')[1:-1]

#%% GA execution

if algorithm in ['ga','both']:

    # GA input
    gn = 1
    gl = kns_size
    popsize = int(kns_size*npsz)
    pm = npm/kns_size
    inputs_ga = inputs_kns + ' ' + str([gn, gl, popsize, pc, pm]).replace(',',' ')[1:-1]

    n_iter = np.array([])
    converged = 0
    no_converged = 0

    for i in range(n_exe):
        print('GA iteration: {}\r'.format(i), end="")
        result = execute_java('ga.ssGA.Exe', inputs_ga)
        if 'No convergence' in result:
            no_converged += 1
        elif 'Stack' in result:
            print(result)
            sys.exit()
        else:
            converged += 1
            n_iter = np.append(n_iter,int(result.split()[-2]))

    # print results
    print('-------------------\nGENETIC ALGORITHM:\n')
    if n_iter.size > 0:
        print('Average iterations = {}\nMin = {}\nMax = {}'.format(np.round(np.mean(n_iter),2),int(min(n_iter)),int(max(n_iter))))
        print('Converged executions: {}\nNon converged executions: {}\nConvergence rate: {}%'.format(converged,no_converged,np.round(100*converged/n_exe,2)))
    else:
        print('No convergence found in any execution')

#%% SA execution

if algorithm in ['sa','both']:

    # SA input
    temp = ntemp * max(kns_p)
    inputs_sa = inputs_kns + ' ' + str([temp, alpha]).replace(',',' ')[1:-1]

    n_iter = np.array([])
    converged = 0
    no_converged = 0

    for i in range(n_exe):
        print('SA iteration: {}\r'.format(i), end="")
        result = execute_java('sa.Exe', inputs_sa)
        if 'No convergence' in result:
            no_converged += 1
        elif 'Stack' in result:
            print(result)
            sys.exit()
        else:
            converged += 1
            n_iter = np.append(n_iter,int(result.split()[-2]))

    # print results
    print('-------------------\nSIMULATED ANNEALING:\n')
    if n_iter.size > 0:
        print('Average iterations = {}\nMin = {}\nMax = {}'.format(np.round(np.mean(n_iter),2),int(min(n_iter)),int(max(n_iter))))
        print('Converged executions: {}\nNon converged executions: {}\nConvergence rate: {}%'.format(converged,no_converged,np.round(100*converged/n_exe,2)))
    else:
        print('No convergence found in any execution')


#%% Execution of all problem instances using both algorithms

if algorithm == 'all':

    df_all = pd.DataFrame(columns=['instance','ga min','ga max','ga mean','ga converged','sa min','sa max','sa mean','sa converged'])

    for i in range(len(os.listdir(problem_dir))):

        print('--------------------\n--------------------\nInstance {} of {}\n'.format(i+1,len(os.listdir(problem_dir))))
        
        instance_dir = os.path.join(problem_dir,os.listdir(problem_dir)[i])
        result_dict = {'instance': os.path.basename(instance_dir)[:-4]}

        # Multidemnsional knapsack problem instance
        txt_file = open(instance_dir,'r')
        txt_lines = txt_file.readlines()
        txt_file.close()

        kns_size = int(txt_lines[0].split()[0])
        kns_ncons = int(txt_lines[0].split()[1])
        kns_solution = float(txt_lines[0].split()[2])
        kns_p = [float(i) for i in list(txt_lines[1].split())]
        kns_capacity = [float(i) for i in list(txt_lines[-1].split())]

        kns_w = []
        for i in range(1,len(txt_lines)-1):
            kns_w.append([float(j) for j in list(txt_lines[i].split())])

        # Flatten list of list
        kns_w = [item for sublist in kns_w for item in sublist]

        inputs_kns = str([kns_size, kns_ncons, kns_solution, kns_penalty] + kns_p + kns_capacity + kns_w).replace(',',' ')[1:-1]

        ## GA
        gn = 1
        gl = kns_size
        popsize = int(kns_size*npsz)
        pm = npm/kns_size
        inputs_ga = inputs_kns + ' ' + str([gn, gl, popsize, pc, pm]).replace(',',' ')[1:-1]

        n_iter = np.array([])
        converged = 0
        no_converged = 0

        for i in range(n_exe):
            print('GA iteration: {}\r'.format(i), end="")
            result = execute_java('ga.ssGA.Exe', inputs_ga)
            if 'No convergence' in result:
                no_converged += 1
            elif 'Stack' in result:
                print(result)
                sys.exit()
            else:
                converged += 1
                n_iter = np.append(n_iter,int(result.split()[-2]))

        # print results
        print('-------------------\nGENETIC ALGORITHM:\n')
        if n_iter.size > 0:
            print('Average iterations = {}\nMin = {}\nMax = {}'.format(np.round(np.mean(n_iter),2),int(min(n_iter)),int(max(n_iter))))
            print('Converged executions: {}\nNon converged executions: {}\nConvergence rate: {}%'.format(converged,no_converged,np.round(100*converged/n_exe,2)))
            result_dict['ga min'] = int(min(n_iter))
            result_dict['ga max'] = int(max(n_iter))
            result_dict['ga mean'] = np.mean(n_iter)
            result_dict['ga converged'] = 100*converged/n_exe
        else:
            print('No convergence found in any execution')
            result_dict['ga converged'] = 0



        # SA input
        temp = ntemp * max(kns_p)
        inputs_sa = inputs_kns + ' ' + str([temp, alpha]).replace(',',' ')[1:-1]

        n_iter = np.array([])
        converged = 0
        no_converged = 0

        for i in range(n_exe):
            print('SA iteration: {}\r'.format(i), end="")
            result = execute_java('sa.Exe', inputs_sa)
            if 'No convergence' in result:
                no_converged += 1
            elif 'Stack' in result:
                print(result)
                sys.exit()
            else:
                converged += 1
                n_iter = np.append(n_iter,int(result.split()[-2]))

        # print results
        print('-------------------\nSIMULATED ANNEALING:\n')
        if n_iter.size > 0:
            print('Average iterations = {}\nMin = {}\nMax = {}'.format(np.round(np.mean(n_iter),2),int(min(n_iter)),int(max(n_iter))))
            print('Converged executions: {}\nNon converged executions: {}\nConvergence rate: {}%'.format(converged,no_converged,np.round(100*converged/n_exe,2)))
            result_dict['sa min'] = int(min(n_iter))
            result_dict['sa max'] = int(max(n_iter))
            result_dict['sa mean'] = np.mean(n_iter)
            result_dict['sa converged'] = 100*converged/n_exe
        else:
            print('No convergence found in any execution')
            result_dict['sa converged'] = 0

        df_all = df_all.append(result_dict, ignore_index=True)

    df_all.to_csv('result/result.csv')

#%% Tuning

if algorithm == 'tuning_sa':

    df_sa = pd.DataFrame(columns=['temp','alpha','instance','min','mean','converged'])

    for i in range(len(os.listdir(problem_dir))):

        print('--------------------\n--------------------\nInstance {} of {}\n'.format(i+1,len(os.listdir(problem_dir))))
        
        instance_dir = os.path.join(problem_dir,os.listdir(problem_dir)[i])
        result_dict = {'instance': os.path.basename(instance_dir)[:-4]}

        # Multidemnsional knapsack problem instance
        txt_file = open(instance_dir,'r')
        txt_lines = txt_file.readlines()
        txt_file.close()

        kns_size = int(txt_lines[0].split()[0])
        kns_ncons = int(txt_lines[0].split()[1])
        kns_solution = float(txt_lines[0].split()[2])
        kns_p = [float(i) for i in list(txt_lines[1].split())]
        kns_capacity = [float(i) for i in list(txt_lines[-1].split())]

        kns_w = []
        for i in range(1,len(txt_lines)-1):
            kns_w.append([float(j) for j in list(txt_lines[i].split())])

        # Flatten list of list
        kns_w = [item for sublist in kns_w for item in sublist]

        inputs_kns = str([kns_size, kns_ncons, kns_solution, kns_penalty] + kns_p + kns_capacity + kns_w).replace(',',' ')[1:-1]

        print('-------------------\nSIMULATED ANNEALING:\n')

        ti = 0

        for temp in sa_tuning['temp']:
            
            result_dict['temp'] = temp

            for alpha in sa_tuning['alpha']:

                ti += 1

                print('-------\n tuning {} of {}\ntemp = {}\nalpha = {}\n'.format(ti,len(sa_tuning['temp']*len(sa_tuning['alpha'])),temp,alpha))

                result_dict['alpha'] = alpha

                # SA input
                inputs_sa = inputs_kns + ' ' + str([temp, alpha]).replace(',',' ')[1:-1]

                n_iter = np.array([])
                converged = 0
                no_converged = 0

                for i in range(n_exe):
                    print('SA iteration: {}\r'.format(i), end="")
                    result = execute_java('sa.Exe', inputs_sa)
                    if 'No convergence' in result:
                        no_converged += 1
                    elif 'Stack' in result:
                        print(result)
                        sys.exit()
                    else:
                        converged += 1
                        n_iter = np.append(n_iter,int(result.split()[-2]))

                # print results
                if n_iter.size > 0:
                    print('Average iterations = {}\nMin = {}\nMax = {}'.format(np.round(np.mean(n_iter),2),int(min(n_iter)),int(max(n_iter))))
                    print('Converged executions: {}\nNon converged executions: {}\nConvergence rate: {}%'.format(converged,no_converged,np.round(100*converged/n_exe,2)))
                    result_dict['min'] = int(min(n_iter))
                    result_dict['max'] = int(max(n_iter))
                    result_dict['mean'] = np.mean(n_iter)
                    result_dict['converged'] = 100*converged/n_exe
                else:
                    print('No convergence found in any execution')
                    result_dict['converged'] = 0

                df_sa = df_sa.append(result_dict, ignore_index=True)

    df_sa.to_csv('result/sa_tuning_result.csv')


elif algorithm == 'tuning_ga':

    df_ga = pd.DataFrame(columns=['npsz','pc','npm','instance','min','mean','converged'])

    for i in range(len(os.listdir(problem_dir))):

        print('--------------------\n--------------------\nInstance {} of {}\n'.format(i+1,len(os.listdir(problem_dir))))
        
        instance_dir = os.path.join(problem_dir,os.listdir(problem_dir)[i])
        result_dict = {'instance': os.path.basename(instance_dir)[:-4]}

        # Multidemnsional knapsack problem instance
        txt_file = open(instance_dir,'r')
        txt_lines = txt_file.readlines()
        txt_file.close()

        kns_size = int(txt_lines[0].split()[0])
        kns_ncons = int(txt_lines[0].split()[1])
        kns_solution = float(txt_lines[0].split()[2])
        kns_p = [float(i) for i in list(txt_lines[1].split())]
        kns_capacity = [float(i) for i in list(txt_lines[-1].split())]

        kns_w = []
        for i in range(1,len(txt_lines)-1):
            kns_w.append([float(j) for j in list(txt_lines[i].split())])

        # Flatten list of list
        kns_w = [item for sublist in kns_w for item in sublist]

        inputs_kns = str([kns_size, kns_ncons, kns_solution, kns_penalty] + kns_p + kns_capacity + kns_w).replace(',',' ')[1:-1]

        # GA input (non variables for the kns instance)
        gn = 1
        gl = kns_size

        print('-------------------\nGENETIC ALGORITHM:\n')

        ti = 0

        for npsz in ga_tuning['npsz']:
            
            result_dict['npsz'] = npsz

            for pc in ga_tuning['pc']:

                result_dict['pc'] = pc

                for npm in ga_tuning['npm']:

                    ti += 1

                    print('-------\n tuning {} of {}\nnpsz = {}\npc = {}\nnpm = {}\n'.format(ti,len(ga_tuning['npsz']*len(ga_tuning['pc'])*len(ga_tuning['npm'])),npsz,pc,npm))

                    result_dict['npm'] = npm

                    # GA input (variable)
                    popsize = int(kns_size*npsz)
                    pm = npm/kns_size
                    inputs_ga = inputs_kns + ' ' + str([gn, gl, popsize, pc, pm]).replace(',',' ')[1:-1]

                    n_iter = np.array([])
                    converged = 0
                    no_converged = 0

                    for i in range(n_exe):
                        print('GA iteration: {}\r'.format(i), end="")
                        result = execute_java('ga.ssGA.Exe', inputs_ga)
                        if 'No convergence' in result:
                            no_converged += 1
                        elif 'Stack' in result:
                            print(result)
                            sys.exit()
                        else:
                            converged += 1
                            n_iter = np.append(n_iter,int(result.split()[-2]))

                    # print results
                    if n_iter.size > 0:
                        print('Average iterations = {}\nMin = {}\nMax = {}'.format(np.round(np.mean(n_iter),2),int(min(n_iter)),int(max(n_iter))))
                        print('Converged executions: {}\nNon converged executions: {}\nConvergence rate: {}%'.format(converged,no_converged,np.round(100*converged/n_exe,2)))
                        result_dict['min'] = int(min(n_iter))
                        result_dict['max'] = int(max(n_iter))
                        result_dict['mean'] = np.mean(n_iter)
                        result_dict['converged'] = 100*converged/n_exe
                    else:
                        print('No convergence found in any execution')
                        result_dict['converged'] = 0

                    df_ga = df_ga.append(result_dict, ignore_index=True)

    df_ga.to_csv('result/ga_tuning_result.csv')

#%% Plotting results

if algorithm == 'plot':

    dfp = pd.read_csv(os.path.join('result','result.csv'))

    dfp.plot.bar(x='instance', y=['ga mean','sa mean'])
    plt.title('Average iterations')
    plt.grid(alpha=0.2)
    plt.show()

    dfp.plot.bar(x='instance', y=['ga converged','sa converged'])
    plt.title('Convergence rate [%]')
    plt.grid(alpha=0.2)
    plt.show()

elif algorithm == 'plot_tuning_sa':

    df = pd.read_csv(os.path.join('result','sa_tuning_result.csv'))
    
    result_converged = []
    result_mean = []

    for temp in df['temp'].unique():

        print(temp)
        
        result_mean.append([])
        result_converged.append([])
        
        for alpha in df['alpha'].unique():
            print(alpha)            
            print(np.mean(df['converged'][(df['temp'] == temp) & (df['alpha'] == alpha)]))
            result_converged[-1].append(np.mean(df['converged'][(df['temp'] == temp) & (df['alpha'] == alpha)]))
            print(result_converged)
            result_mean[-1].append(np.mean(df['mean'][(df['temp'] == temp) & (df['alpha'] == alpha)].dropna()))

    # Convergence plot
    result = np.array(result_converged, dtype=np.float)
    fig=plt.figure()
    ax1=fig.add_subplot(111, projection='3d')
    ax1.set_xlabel('ntemp', labelpad=10)
    ax1.set_ylabel('\u03B1', labelpad=10)
    ax1.set_zlabel('Congercence rate')
    xlabels = np.array(list(df['temp'].unique()))
    xpos = np.arange(xlabels.shape[0])
    ylabels = np.array(list(df['alpha'].unique()))
    ypos = np.arange(ylabels.shape[0])
    
    xposM, yposM = np.meshgrid(xpos, ypos, copy=False)
    
    zpos=result
    zpos = zpos.ravel()
    
    dx=0.5
    dy=0.5
    dz=zpos
    
    ax1.w_xaxis.set_ticks(xpos + dx/2.)
    ax1.w_xaxis.set_ticklabels(xlabels)
    
    ax1.w_yaxis.set_ticks(ypos + dy/2.)
    ax1.w_yaxis.set_ticklabels(ylabels)
    
    values = np.linspace(0.2, 1., xposM.ravel().shape[0])
    colors = cm.rainbow(values)
    ax1.bar3d(xposM.ravel(), yposM.ravel(), dz*0, dx, dy, dz)
    plt.show()
    fig.savefig(os.path.join('figures','tuning_sa_convergence.png'), bbox_inches='tight', dpi=300)

    # Mean iterations plot
    result = np.array(result_mean, dtype=np.float)
    fig=plt.figure()
    ax1=fig.add_subplot(111, projection='3d')
    ax1.set_xlabel('ntemp', labelpad=10)
    ax1.set_ylabel('\u03B1', labelpad=10)
    ax1.set_zlabel('Mean iterations')
    xlabels = np.array(list(df['temp'].unique()))
    xpos = np.arange(xlabels.shape[0])
    ylabels = np.array(list(df['alpha'].unique()))
    ypos = np.arange(ylabels.shape[0])
    
    xposM, yposM = np.meshgrid(xpos, ypos, copy=False)
    
    zpos=result
    zpos = zpos.ravel()
    
    dx=0.5
    dy=0.5
    dz=zpos
    
    ax1.w_xaxis.set_ticks(xpos + dx/2.)
    ax1.w_xaxis.set_ticklabels(xlabels)
    
    ax1.w_yaxis.set_ticks(ypos + dy/2.)
    ax1.w_yaxis.set_ticklabels(ylabels)
    
    values = np.linspace(0.2, 1., xposM.ravel().shape[0])
    colors = cm.rainbow(values)
    ax1.bar3d(xposM.ravel(), yposM.ravel(), dz*0, dx, dy, dz)
    plt.show()
    fig.savefig(os.path.join('figures','tuning_sa_iterations.png'), bbox_inches='tight', dpi=300)

elif algorithm == 'plot_tuning_ga':

    df = pd.read_csv(os.path.join('result','ga_tuning_result.csv'))

    for npm in df['npm'].unique():

        result_converged = []
        result_mean = []

        for npsz in df['npsz'].unique():
            
            result_converged.append([])
            result_mean.append([])
            
            for pc in df['pc'].unique():
                
                result_converged[-1].append(np.mean(df['converged'][(df['npsz'] == npsz) & (df['pc'] == pc)]))
                result_mean[-1].append(np.mean(df['mean'][(df['npsz'] == npsz) & (df['pc'] == pc)].dropna()))
        
        # Convergence plot
        result = np.array(result_converged, dtype=np.float)
        fig=plt.figure()
        plt.title('npm = {}'.format(npm))
        plt.axis('off')
        ax1=fig.add_subplot(111, projection='3d')
        ax1.set_xlabel('npsz', labelpad=10)
        ax1.set_ylabel('pc', labelpad=10)
        ax1.set_zlabel('Congercence rate')
        xlabels = np.array(list(df['npsz'].unique()))
        xpos = np.arange(xlabels.shape[0])
        ylabels = np.array(list(df['pc'].unique()))
        ypos = np.arange(ylabels.shape[0])
        
        xposM, yposM = np.meshgrid(xpos, ypos, copy=False)
        
        zpos=result
        zpos = zpos.ravel()
        
        dx=0.5
        dy=0.5
        dz=zpos
        
        ax1.w_xaxis.set_ticks(xpos + dx/2.)
        ax1.w_xaxis.set_ticklabels(xlabels)
        
        ax1.w_yaxis.set_ticks(ypos + dy/2.)
        ax1.w_yaxis.set_ticklabels(ylabels)
        
        values = np.linspace(0.2, 1., xposM.ravel().shape[0])
        ax1.bar3d(xposM.ravel(), yposM.ravel(), dz*0, dx, dy, dz)
        plt.show()
        fig.savefig(os.path.join('figures','tuning_ga_convergence_npm_{}.png'.format(npm,npsz,pc)), bbox_inches='tight', dpi=300)

        # Mean iterations plot
        result = np.array(result_mean, dtype=np.float)
        fig=plt.figure()
        plt.title('npm = {}'.format(npm))
        plt.axis('off')
        ax1=fig.add_subplot(111, projection='3d')
        ax1.set_xlabel('npsz', labelpad=10)
        ax1.set_ylabel('pc', labelpad=10)
        ax1.set_zlabel('Mean iterations')
        xlabels = np.array(list(df['npsz'].unique()))
        xpos = np.arange(xlabels.shape[0])
        ylabels = np.array(list(df['pc'].unique()))
        ypos = np.arange(ylabels.shape[0])
        
        xposM, yposM = np.meshgrid(xpos, ypos, copy=False)
        
        zpos=result
        zpos = zpos.ravel()
        
        dx=0.5
        dy=0.5
        dz=zpos
        
        ax1.w_xaxis.set_ticks(xpos + dx/2.)
        ax1.w_xaxis.set_ticklabels(xlabels)
        
        ax1.w_yaxis.set_ticks(ypos + dy/2.)
        ax1.w_yaxis.set_ticklabels(ylabels)
        
        values = np.linspace(0.2, 1., xposM.ravel().shape[0])
        ax1.bar3d(xposM.ravel(), yposM.ravel(), dz*0, dx, dy, dz)
        plt.show()
        fig.savefig(os.path.join('figures','tuning_ga_iterations_npm_{}.png'.format(npm,npsz,pc)), bbox_inches='tight', dpi=300)
