
package ga.ssGA;

public class Knapsack extends Problem{

    public double Evaluate(Individual Indiv, double[] p, double[][] w, double[] c, int pen, int n_cons) {
      return FITNESS(Indiv, p, w, c, pen, n_cons) ;
    }

    private double FITNESS(Individual indiv, double[] p, double[][] w, double[] c, int pen, int n_cons)
    {

      double f=0.0;
      double[] weight = new double[n_cons];

      if(CL!=p.length)	
      System.out.println("Length mismatch error in Subset sum function.");

      for(int i=0; i<CL; i++){
        f += indiv.get_allele(i)*p[i];
        for(int j=0; j<n_cons; j++)
          weight[j] += indiv.get_allele(i)*w[j][i];
      }
      for(int i=0; i<n_cons; i++)
        if (weight[i] > c[i]) {
          if (pen == 0){f = 0.0;}
          if (pen == 1){f = c[i] - f*0.1;}
        }
      indiv.set_fitness(f);
      return f;
    }

}