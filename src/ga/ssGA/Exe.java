///////////////////////////////////////////////////////////////////////////////
///            Steady State Genetic Algorithm v1.0                          ///
///                by Enrique Alba, July 2000                               ///
///                                                                         ///
///   Executable: set parameters, problem, and execution details here       ///
///////////////////////////////////////////////////////////////////////////////

package ga.ssGA;

public class Exe
{
  public static void main(String args[]) throws Exception
  {

    try{
    int kns_size = Integer.parseInt(args[0]);
    int n_cons = Integer.parseInt(args[1]);
    double tf = Double.parseDouble(args[2]);
    int kns_penalty = Integer.parseInt(args[3]);
    double[] p = new double[kns_size];
    for(int i=0; i<kns_size; i++){p[i] = Double.parseDouble(args[4+i]);}
    double[] kns_capacity = new double[n_cons];
    for(int i=0; i<n_cons; i++){kns_capacity[i] = Double.parseDouble(args[4+kns_size+i]);}
    double[][] w = new double[n_cons][kns_size];
    for(int i=0; i<n_cons; i++)
        for(int j=0; j<kns_size; j++)
        w[i][j] = Double.parseDouble(args[4+kns_size+n_cons+kns_size*(i+1)+j]);

    // PARAMETERS GA
    int    index_ga   = 4 + kns_size + n_cons + kns_size*(n_cons + 1);
    int    gn         = Integer.parseInt(args[index_ga]);       // Gene number
    int    gl         = Integer.parseInt(args[index_ga+1]);     // Gene length
    int    popsize    = Integer.parseInt(args[index_ga+2]);     // Population size
    double pc         = Double.parseDouble(args[index_ga+3]);   // Crossover probability
    double pm         = Double.parseDouble(args[index_ga+4]);   // Mutation probability
    long   MAX_ISTEPS = 50000;
    Boolean converged = false;
    
    Problem   problem;                             // The problem being solved
    problem = new Knapsack(); 

    // double tf = problem.Get_best_solution() ;           // Target fitness being sought
    
    problem.set_geneN(gn);
    problem.set_geneL(gl);
    problem.set_target_fitness(tf);

    Algorithm ga;          // The ssGA being used
    ga = new Algorithm(problem, popsize, gn, gl, pc, pm, p, w, kns_capacity, kns_penalty, n_cons);


    for (int step=0; step<MAX_ISTEPS; step++)
    {  
      ga.go_one_step(p, w, kns_capacity, kns_penalty, n_cons);

      if(     (problem.tf_known())                    &&
      (ga.get_solution()).get_fitness()>=problem.get_target_fitness()
      )
      { 
        System.out.print("Solutions found after: "); 
        System.out.print(step + 1);
        System.out.print(" iterations"); 
        converged = true;
        break;
      }

    }

    if(!converged)
      System.out.printf("No convergence"); System.out.println();

  } catch(Exception e) {
    System.out.println("toString(): " + e.toString());
    System.out.println("getMessage(): " + e.getMessage());
    System.out.println("StackTrace: " + e.getStackTrace()[0].getLineNumber());
  }
  }

}
// END OF CLASS: Exe
