package sa;

import java.util.Random;
import java.util.Arrays;
import java.lang.Math;

public class Exe {
    public static void main(String args[]) throws Exception
    {

        try{

        int kns_size = Integer.parseInt(args[0]);
        int n_cons = Integer.parseInt(args[1]);
        double tf = Double.parseDouble(args[2]);
        int kns_penalty = Integer.parseInt(args[3]);
        double[] p = new double[kns_size];
        for(int i=0; i<kns_size; i++){p[i] = Double.parseDouble(args[4+i]);}
        double[] kns_capacity = new double[n_cons];
        for(int i=0; i<n_cons; i++){kns_capacity[i] = Double.parseDouble(args[4+kns_size+i]);}
        double[][] w = new double[n_cons][kns_size];
        for(int i=0; i<n_cons; i++)
            for(int j=0; j<kns_size; j++)
                w[i][j] = Double.parseDouble(args[4+kns_size+n_cons+kns_size*(i+1)+j]);
                
        double temp = Double.parseDouble(args[4+kns_size+n_cons+kns_size*(n_cons+1)]);
        double alpha = Double.parseDouble(args[5+kns_size+n_cons+kns_size*(n_cons+1)]);

        long   MAX_ISTEPS = 50000;
        Boolean converged = false;

        int[] sol_actual = initial_solution(kns_size, w, kns_capacity, n_cons, kns_size);
        double f_actual = fitness(sol_actual, p, w, kns_size, n_cons, kns_capacity, kns_penalty);

        int[] sol_new;
        double f_new;
        double diff;
        double prob_acc;
        Random r = new Random();


        for (int step=0; step<MAX_ISTEPS; step++){

            sol_new = next_solution(sol_actual, w, kns_capacity, n_cons, kns_size);
            f_new = fitness(sol_new, p, w, kns_size, n_cons, kns_capacity, kns_penalty);
            if (f_new >= f_actual){
                sol_actual = Arrays.copyOf(sol_new, sol_new.length);
                f_actual = f_new;
            }
            else {
                diff = f_new - f_actual;
                prob_acc = Math.exp(diff/temp);
                if(r.nextDouble() < prob_acc){
                    sol_actual = Arrays.copyOf(sol_new, sol_new.length);
                    f_actual = f_new;
                }
            }
            if (f_actual >= tf) {
                System.out.print("Solutions found after: "); 
                System.out.print(step + 1);
                System.out.print(" iterations"); 
                converged = true;
                break;}

            System.out.print(step); System.out.println();
            temp *= alpha;
        }

        if(!converged)
            System.out.printf("No convergence"); System.out.println();

        } catch(Exception e) {
            System.out.println("toString(): " + e.toString());
            System.out.println("getMessage(): " + e.getMessage());
            System.out.println("StackTrace: "+ e.getStackTrace()[0].getLineNumber());
         }

    }

    private static int[] initial_solution(int length, double[][] w, double c[], int n_cons, int kns_size)
    {
        Random r = new Random();
        int[] sol_ini = new int[length];
        double[] weight = new double[n_cons];
        Boolean feas = true;
        int index = 0;
        Arrays.fill(weight, 0);

        while(feas){
            index = r.nextInt(sol_ini.length);
            if (sol_ini[index] == 0){
                sol_ini[index] = 1;
                feas = check_feasible(sol_ini, w, c, n_cons, kns_size);
            }
        }

        sol_ini[index] = 0;

        return sol_ini;
    }

    private static double fitness(int[] sol, double[] p, double[][] w, int size, int n_cons, double[] c, int pen)
    {
        double f=0.0;
        double[] weight = new double[n_cons];
        Arrays.fill(weight, 0);

        for(int i=0; i<size; i++){
            f += sol[i]*p[i];
            for (int j=0; j<n_cons; j++){
                weight[j] += sol[i]*w[j][i];
            }
        }

        // Penalty
        for(int i=0; i<n_cons; i++){
            if (weight[i] > c[i]) {
                if (pen == 0){f = 0.0;}
                if (pen == 1){f = c[i] - f*0.1;}
            }
        }
        return f;
    }

    private static int[] next_solution(int[] sol_0, double[][] w, double c[], int n_cons, int kns_size){
        Random r = new Random();
        int index_1;
        Boolean feas = false;
        int[] sol_1 = Arrays.copyOf(sol_0, sol_0.length);
        
        while(!feas){
            index_1 = r.nextInt(sol_0.length);
            if (sol_1[index_1] == 1)
            sol_1[index_1] = 0;
            else
            sol_1[index_1] = 1;
            feas = check_feasible(sol_1, w, c, n_cons, kns_size);
        }

        return sol_1;
    }

    private static Boolean check_feasible(int[] sol, double[][] w, double c[], int n_cons, int kns_size){

        Boolean feas = true;
        double weight;

        for (int i=0; i<n_cons; i++){
            weight = 0.0;
            for (int j=0; j<kns_size; j++){
                if(sol[j] == 1)
                    weight += w[i][j];
                }
            if (weight > c[i]){feas = false;}
        }

        return feas;
    }

}
    
