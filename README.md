# metaheuristic_knapsack

Comparison between a genetic algorithm and simulated annealing applied to multidimensional knapsack problem

## Usage

### Inputs

The code has 5 modes that can be selected by modifying the varaiable *algorithm*:

- 'sa': Executes Simulated Annealing *n_exe* times for the selected problem instance (*problem_dir*, *instance_dir*).
- 'ga': Executes Genetic Algorithm *n_exe* times for the selected problem instance (*problem_dir*, *instance_dir*).
- 'both': Executes both SA and GA *n_exe* times for the selected problem instance (*problem_dir*, *instance_dir*).
- 'all': Executes both SA and GA *n_exe* times for all the problem instances in *problem_dir*.
- 'plot': Plot the results obtained after the execution of mode 'all'.
- 'tuning_sa': Executes SA *n_exe* times for all the problem instances  in *problem_dir* and all the SA parameters in *sa_tuning* dictionary.
- 'tuning_ga': Executes GA *n_exe* times for all the problem instances  in *problem_dir* and all the GA parameters in *ga_tuning* dictionary.
- 'plot_tuning_sa': Plot the results obtained after the execution of mode 'tuning_sa'.
- 'plot_tuning_ga': Plot the results obtained after the execution of mode 'tuning_ga'.

In the input section, the parameters for SA (*ntemp*, *alpha*) and GA (*npm*, *npsz*, *pc*) can be selected for the modes 'sa', 'ga', 'both' and 'all'. Also the dictionaries *sa_tuning* and *ga_tuning* can be filled with any number of parameters for the modes 'tuning_sa' and 'tuning_ga'.

# Execution

After setting the inputs, to run the program open a terminal and do:

```
python test.py
```

The python script will also compile and call the execution of the java files.
